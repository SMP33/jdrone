# JDrone

## Install for Linux:
git clone https://gitlab.com/SMP33/jdrone.git  
cd jdrone  
mkdir build  
cd build  
cmake ..  
make  
sudo make install  

## Usage:

find_package( JDrone REQUIRED )  
include_directories( ${JDrone_INCLUDE_DIRS} )  

add_executable( test test.cpp ) 
 
target_link_libraries( test JDrone::Utils )  
target_link_libraries( test JDrone::Telemetry )  
target_link_libraries( test JDrone::FlyingPlatform)  
