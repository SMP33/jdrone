#include "arucomapdetector.h"
#include <qfile.h>
#include <qthread.h>
#include <chrono>
#include <QSettings>
#include <QList>
#include <QVariant>
#include <QVariantList>
#include <QStringList>

using namespace std;
using namespace cv;
using namespace GM;


Mat Q2R(GM::Quat q)
{
    float x = q.x;
    float y = q.y;
    float z = q.z;
    float w = q.w;

    return Mat1f(3, 3) << (1 - 2 * y * y - 2 * z * z), (2 * x * y - 2 * z * w), (2 * x * z + 2 * y * w),
        (2 * x * y + 2 * z * w), (1 - 2 * x * x - 2 * z * z), (2 * y * z - 2 * x * w),
        (2 * x * z - 2 * y * w), (2 * y * z + 2 * x * w), (1 - 2 * x * x - 2 * y * y);
}

Point3f createPoint(Mat R, float x, float y, float z, Mat tvec)
{
    Mat p = R * (cv::Mat_<float>(3, 1) << (x, y, z))+tvec;

    return { p.at<float>(0),p.at<float>(1), p.at<float>(2) };
}

#include <sstream>

std::string to_str2( float f)
{
    char str[40];
    sprintf(str, "%.2f", f);
    return string(str);
}

ArucoMapDetector::ArucoMapDetector(AppProperty props, QObject* parent) :
    QObject(parent),
    props(props),
    cap(nullptr),
    markerDictionary(aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME::DICT_4X4_1000)),
    parameters(aruco::DetectorParameters::create()),
    cameraOffset(V3(0,0,0)),
    max_mrk_quality(4),
    fps(0),
    arucoData(AcfPosition(0,0,0,0)),
    yaw(0),
    frame(Mat(480,640, CV_8UC3, Scalar(0, 0, 0))),
    outFrame(Mat(480, 640, CV_8UC3, Scalar(0, 0, 0)))
{


    //Параметры из props
    vector<float> camera_matrix_values(9);
    QStringList camera_matrix_str=props.props["CAMERA_MATRIX"].split(",");

    for(int i=0;i<9;i++)
    {
        camera_matrix_values[i]=camera_matrix_str[i].toFloat();

    }

    camera_matrix=Mat1f(3,3);
    memcpy(camera_matrix.data,camera_matrix_values.data(),
           camera_matrix_values.size()*sizeof(float));

    QStringList dist_coeffs_str=props.props["CAMERA_DISTANCE_COEFFICIENTS"].split(",");

    dist_coeffs=Mat1f(dist_coeffs_str.size(),1);
    vector<float> dist_coeffs_values(dist_coeffs_str.size());

    for(int i=0;i<dist_coeffs_str.size();i++)
    {
        dist_coeffs_values[i]=dist_coeffs_str[i].toFloat();
    }

    memcpy(dist_coeffs.data,dist_coeffs_values.data(),
           dist_coeffs_values.size()*sizeof(float));

    if (props.props.contains("CAMERA_OFFSET"))
    {
        QStringList cameraOffset_str = props.props["CAMERA_OFFSET"].split(",");

        cameraOffset = V3(cameraOffset_str[0].toFloat(),
            cameraOffset_str[1].toFloat(),
            cameraOffset_str[2].toFloat());

        cout << "Camera offset: " << cameraOffset.toStr() << endl;
    }

    loadMarkerMap();

}

void ArucoMapDetector::loadMarkerMap()
{

    float scale=props.props["ARUCO_MAP_SCALE"].toFloat();

    QString path=props.props["EXECUTABLE_PATH"]+"/"+props.props["ARUCO_MAP_PATH"];
    path=path.trimmed();

    QFile f(path);

    if(!f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        cout<<"File was not open!!!"<<endl;
        exit(-1);
    }

    while (!f.atEnd())
    {
        QString line = f.readLine();
        if(line.count(",")==4)
        {
            QStringList data=line.split(",");

            int id=data[0].toInt();
            double x=data[1].toDouble()*scale;
            double y=data[2].toDouble()*scale;
            float size = data[3].toFloat() * scale;

            SingleMapMarker m(size,GM::V3(x,y,0));
            mrks.insert(id,m);
        }
    }
    qDebug()<< "Markers are loaded";
}

void ArucoMapDetector::start()
{
    qDebug()<< "Try to start capture...";
    cap=new VideoCapture(0);
    cap->set(cv::CAP_PROP_FPS, 40);
    if (!cap->isOpened())
    {
        qDebug()<< "Error opening video stream or file";
        exit(-1);
    }
    else{
        qDebug()<< "Capture opened!";
    }

    parameters->adaptiveThreshWinSizeMin = 10;
    long long time = 0;
    int i = 0;
    auto lastSendTime = std::chrono::high_resolution_clock::now();
    
    bool sendImg = true;

    int count=0;
    QElapsedTimer FPSTimer;
    QElapsedTimer OutTimer;
    FPSTimer.start();
    OutTimer.start();

    while (true)
    {
        QThread::usleep(100);
        process(sendImg);

        if (OutTimer.nsecsElapsed()>1e8)
        {
            sendImg = true;
            OutTimer.restart();
        }
        else
            sendImg = false;

        count++;
        if (FPSTimer.nsecsElapsed() > 1e9)
        {
            FPSTimer.restart();
            fps = count;

            count = 0;
        }

    }
}

void ArucoMapDetector::process(bool sendImg)
{
    cap->read(frame);
    if (!frame.empty())
    {
       markerIds.clear();
       markerCorners.clear();
       rejectedCandidates.clear();
       buff.clear();

        aruco::detectMarkers(frame, markerDictionary, markerCorners, markerIds,parameters);
     
        vector<GM::Quat> quats;

        for(int i=0;i<markerCorners.size();i++)
        {
            if (mrks.find(markerIds[i])==mrks.end()) //Маркер не найден в списке
                {
                    vector<vector<cv::Point2f>> corners_ = { markerCorners[i]};
                    vector<int> ids_ = { markerIds[i]};
                    aruco::drawDetectedMarkers(frame, corners_, ids_, Scalar(0,0,255));
                    continue;
                }
            else
            {
                vector<cv::Point2f> corn(markerCorners[i]);
                vector<cv::Vec3d> rvecs, tvecs, euler;
                vector<vector<cv::Point2f>> corners(0);
                corners.push_back(corn);

                cv::aruco::estimatePoseSingleMarkers(corners, mrks[markerIds[i]].size,
                                                     camera_matrix,dist_coeffs,
                                                     rvecs,tvecs);
     
                vector<vector<cv::Point2f>> corners_ = { markerCorners[i] };
                vector<int> ids_ = { markerIds[i] };

                vector<Point2i> intCorners(0);
                for (const auto& c : markerCorners[i])
                {
                    intCorners.push_back(Point2i(c));
                }

                if(mrks[markerIds[i]].quality<max_mrk_quality)
                    polylines(frame, intCorners, true, { 0,255,255 }, 2);
                else
                {
                    polylines(frame, intCorners, true, { 0,255,0 }, 2);
                }

                Vec3d t_(tvecs[0]);
                Vec3d r_(rvecs[0]);

                GM::V3 t(t_[0], t_[1], t_[2]);
                GM::V3 r(-r_[0], -r_[1], -r_[2]);

                double theta = r.length_xyz();
                r = r/theta;
                double s = sin(theta / 2);

                GM::Quat q(cos(theta / 2), r.x * s, r.y * s,r.z * s);
                quats.push_back(q);
                r = q.toEul();
                t = -(t.rotate(r));

                V3 currentCameraOffset = -(cameraOffset.rotate(r));

                auto res = (t + currentCameraOffset);

                auto& mrk = mrks[markerIds[i]];
                mrk.cPos = res - mrk.bPos;
                mrk.cRot = r;
                mrk.findNow = true;
                mrk.cQuat = q.normalize();

            }
     
     
        }

        V3 pos(0, 0, 0);
        V3 rot(0, 0, 0);

        GM::Quat q_avr(0,0,0,0);

        int count = 0;
        Quat q_ref;


        for (auto& mrk : mrks)
        {
            if (mrk.findNow)
            {
                if (mrk.quality == max_mrk_quality)
                {
                    pos = pos + mrk.cPos;
                    rot = rot + mrk.cRot;

                    if (count == 0)
                    {
                        q_ref = mrk.cQuat;
                    }

                    GM::Quat q = mrk.cQuat;

                    double s = q_ref.w * q.w + q_ref.x * q.x + q_ref.y * q.y + q_ref.z * q.z;

                    if (s < 0)
                    {
                        q = GM::Quat(-q.w, -q.x, -q.y, -q.z);
                    }

                    q_avr = GM::Quat(q_avr.w + q.w,
                        q_avr.x + q.x,
                        q_avr.y + q.y,
                        q_avr.z + q.z);

                    count++;
                }
                if (mrk.quality < max_mrk_quality)
                    mrk.quality++;

                mrk.findNow = false;
            }
            else
            {
                mrk.quality = 0;
            }
        }

        if (count > 0)
        {
            pos = pos / count;
            rot = rot / count;
            q_avr = GM::Quat(q_avr.w / count,
                q_avr.x / count,
                q_avr.y / count,
                q_avr.z / count);

            yaw = q_avr.toEul().z;

            arucoData = ArucoData(AcfPosition(pos.y, pos.x, pos.z, yaw),fps,count);

            emit positionUpdated(arucoData);
        }

        if (sendImg)
        {
            outFrame = frame.clone();

            const int h_step = 11;

            putText(outFrame, ("FPS: " + to_str2(fps)).c_str(), Point2f(1, h_step), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0),1);
            putText(outFrame, ("R: " + to_str2(pos.length_xy())).c_str(), Point2f(1, 3 * h_step), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 1);
            putText(outFrame, ("H: " + to_str2(pos.z)).c_str(), Point2f(1, 5 * h_step), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 1);
            putText(outFrame, ("Psi: " + to_string(int(yaw))).c_str(), Point2f(1, 7 * h_step), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 1);

            putText(outFrame, ("Pos: " + string(GM::V3(pos.y, pos.x, pos.z).toStr())).c_str(), Point2f(1, outFrame.rows-h_step), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 1);
            
            std::vector<uchar> buff;
            cv::imencode(".jpeg", outFrame, buff);
            std::string content(buff.begin(), buff.end());

            VideoFrame img;
            img.type=1;
            img.content=QByteArray::fromStdString(content);

            emit frameUpdated(img);
        }
    }
}








































