#ifndef ARUCOMAPDETECTOR_H
#define ARUCOMAPDETECTOR_H

#include <QObject>
#include <qdebug.h>
#include <qmutex.h>
#include <QByteArray>
#include <qelapsedtimer.h>

#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>

#include <utils/Utils.h>
#include <messages/Messages.h>

#include <GeoMath.h>

struct SingleMapMarker
{
    SingleMapMarker():
        size(0),
        bPos(GM::V3(0, 0, 0)),
        bRot(GM::V3(0, 0, 0))
    {

    }

    SingleMapMarker(float size,GM::V3 position):
        size(size),
        bPos(position)
    {

    }

    float size;
    GM::V3 bPos; //basic position
    GM::V3 bRot; //basic rotatint - not used

    GM::V3 cPos = GM::V3(0, 0, 0); //current position
    GM::V3 cRot = GM::V3(0, 0, 0); //current rotation - eul
    GM::Quat cQuat = GM::Quat(0, 0, 0, 0); //current rotation - quaternion

    int quality = 0; //estimation quality

    bool findNow = false;

};

struct EstimatedMarker
{
    GM::V3 position;
    GM::V3 rotation;
};

class ArucoMapDetector : public QObject
{
    Q_OBJECT
public:
    explicit ArucoMapDetector(AppProperty props,QObject *parent = nullptr);
    
public slots:
    void start();

signals:
    void frameUpdated(const VideoFrame& frame);
    void positionUpdated(const ArucoData& arucoData);

private:
    AppProperty props;
    cv::Mat camera_matrix;
    cv::Mat dist_coeffs;
    QMap<int,SingleMapMarker> mrks;
    int max_mrk_quality;
    cv::VideoCapture* cap;
    cv::Ptr<cv::aruco::Dictionary> markerDictionary;
    cv::Ptr<cv::aruco::DetectorParameters> parameters;
    void process(bool sendImg);
    cv::Mat frame;
    cv::Mat outFrame;
    GM::V3 cameraOffset;
    std::vector<int> markerIds;
    std::vector<std::vector<cv::Point2f>> markerCorners;
    std::vector<std::vector<cv::Point2f>> rejectedCandidates;
    std::vector<uchar> buff;
    int fps;
    ArucoData arucoData;
    double yaw;

    void loadMarkerMap();


};

#endif // ARUCOMAPDETECTOR_H
