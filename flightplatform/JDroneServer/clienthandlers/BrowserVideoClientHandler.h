#ifndef BrowserVideoClientHandler_H
#define BrowserVideoClientHandler_H

#include <QObject>
#include "abstractclienthandler.h"

#include <QString>
#include <messages/Messages.h>

class BrowserVideoClientHandler : public AbstractClientHandler
{
    Q_OBJECT
public:
    BrowserVideoClientHandler(QString title="Video Stream", QObject* parent = nullptr);

    // AbstractClientHandler interface
public slots:
    void read(const QByteArray &bytes);
    void socketClosed();
    void frameUpd(const VideoFrame& frame);
    void start();

private:
    QString title;
    bool isStarted;
    int i=0;
};

#endif // BrowserVideoClientHandler_H
