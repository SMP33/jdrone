#include "BrowserVideoClientHandler.h"
#include <QThread>
#include <QDebug>

BrowserVideoClientHandler::BrowserVideoClientHandler(QString title,QObject* parent):
    AbstractClientHandler(parent),
    title(title),
    isStarted(false)
{

}

void BrowserVideoClientHandler::read(const QByteArray &bytes)
{

}

void BrowserVideoClientHandler::socketClosed()
{
    isStarted=false;
    emit disconnected();
    deleteLater();
}

void BrowserVideoClientHandler::frameUpd(const VideoFrame &frame)
{
    if(isStarted)
    {
        QByteArray boundary = ("--boundary\r\n" \
                            "Content-Type: image/jpeg\r\n" \
                            "Content-Length: ");

        boundary.append(QString::number(frame.content.length()));
        boundary.append("\r\n\r\n");
        boundary.append(frame.content);

        emit write(boundary);
    }


}

void BrowserVideoClientHandler::start()
{
    QByteArray header = ("HTTP/1.0 200 OK\r\n" \
                         "Server: en.code-bude.net example server\r\n" \
                         "Cache-Control: no-cache\r\n" \
                         "Cache-Control: private\r\n" \
                         "Content-Type: multipart/x-mixed-replace;boundary=--boundary\r\n\r\n");

//    QThread* thr=new QThread(this);
//    thr->start();
//    this->moveToThread(thr);

    emit write(header);

    isStarted=true;
}
