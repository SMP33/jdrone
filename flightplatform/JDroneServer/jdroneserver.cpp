#include "jdroneserver.h"
#include <utils/Utils.h>

JDroneServer::JDroneServer(quint16 port,QString main_page,QObject *parent) : QTcpServer(parent),
    port(port),
    main_page(main_page),
    mutex(new QMutex)
{
    setMaxPendingConnections(10);
}

void JDroneServer::start()
{
    assert(this->listen(QHostAddress::Any,port));

    connect(this,&QTcpServer::newConnection,
            this,&JDroneServer::newConnection);
}

void JDroneServer::newConnection()
{
    QTcpSocket* socket = this->nextPendingConnection();

    connect(socket,&QTcpSocket::readyRead,
            this,&JDroneServer::firstReadClient);

    qDebug()<<"New client!";

}

void JDroneServer::firstReadClient()
{
    qDebug()<<"Read client";
    QTcpSocket* socket = (QTcpSocket*)sender();
    QByteArray buf;

    while (socket->bytesAvailable()>0) {
        buf.append(socket->readAll());
    }

    qDebug()<<"Set handler";

    TcpRequest request=extractRequest(buf);
    setClientHandler(request,socket);

    qDebug()<<"Thats all";


    disconnect(socket,&QTcpSocket::readyRead,
            this,&JDroneServer::firstReadClient);
}

void JDroneServer::writeToSocket(const QByteArray &bytes)
{
    AbstractClientHandler* handler=qobject_cast<AbstractClientHandler*>(sender());
    if(handler)
    {
        if(handlers.contains(handler))
        {
            auto& socket=handlers[handler];
            if(socket!=nullptr)
            {
                if(socket->isWritable())
                {
                    socket->write(bytes);
                    qDebug()<<"Wait...";
                    socket->flush();
                }
            }
        }
    }
}

void JDroneServer::closeSocket()
{
    AbstractClientHandler* handler=qobject_cast<AbstractClientHandler*>(sender());
    if(handler)
    {
        if(handlers.contains(handler))
        {
            auto& socket=handlers[handler];
            if(socket!=nullptr)
            {
                socket->close();
                socket->deleteLater();
                socket=nullptr;
            }
        }
    }
}

void JDroneServer::deleteHandlerFromMap()
{
    qDebug()<<"Delete handler...";

    AbstractClientHandler* handler=qobject_cast<AbstractClientHandler*>(sender());
    if(handler)
    {
        handlers.remove(handler);
        qDebug()<<handlers.size() <<"handlers more";
    }else
        qDebug()<<"ERROR";
}

int JDroneServer::getPort() const
{
    QMutexLocker lock(mutex);
    return port;
}

void JDroneServer::setClientHandler(TcpRequest request, QTcpSocket *socket)
{
    if(request.type=="HTTP")
    {
        if(handlers.size()>4)
        {
            QByteArray response= "HTTP/1.1 200 OK\r\n"\
                            "Content-Type: text/html; charset=utf-8\r\n"\
                            "\r\n\r\nСорян, сервачок перегружен(((";

            socket->write(response);
            socket->flush();
            socket->close();
            return;
        }

        if(request.content=="/")
        {
            QByteArray response= "HTTP/1.1 200 OK\r\n"\
                            "Content-Type: text/html; charset=utf-8\r\n"\
                            "\r\n\r\n"+main_page.toUtf8();

            socket->write(response);
            socket->flush();
            socket->close();
        } else if(request.content=="/video_stream.mjpeg"){ //Стрим видео с камеры
            BrowserVideoClientHandler* handler = new BrowserVideoClientHandler("Video Stream");

            handlers.insert(handler,socket);

            connect(handler,&BrowserVideoClientHandler::write,
                    this,&JDroneServer::writeToSocket);

            connect(this,&JDroneServer::frameUpd,
                    handler,&BrowserVideoClientHandler::frameUpd);

            connect(socket, &QTcpSocket::disconnected,
                    handler,&BrowserVideoClientHandler::socketClosed);

            connect(handler,&BrowserVideoClientHandler::disconnected,
                    this,&JDroneServer::deleteHandlerFromMap);

            handler->start();
        } else
        {
            QByteArray response= "HTTP/1.1 200 OK\r\n"\
                                 "Content-Type: text/html; charset=utf-8\r\n"\
                                 "\r\n\r\n"\
                                 "<html>\r\n"\
                                 "<head>"\
                                  "<title>Go out of here!</title>"\
                                 "<style>"\
                                 ".text {"\
                                 "text-align:  center;"\
                                 "}"\
                                 "</style>"\
                                 "</head>"\
                                 "<body>"\
                                 "<div class=\"text\">"\
                                 "<h1>404</h1>"\
                                 "<h3>Watafak are you doing maaaaan?<br>"\
                                 "What is the</h3><h2>"+request.content.toUtf8()+"?</h2><h3><br>"\
                                 "Get out, this is not the page you need!<br><br>"
                                 "<a href=\"/\">Main page<a></h3>"\
                                 "</div>"\
                                 "</body>"\
                                 "</html>";

            socket->write(response);
            socket->flush();
            socket->close();
        }

    } else if (request.type=="API_QT") {

    } else {
        socket->close();
    }
}

TcpRequest JDroneServer::extractRequest(QString str)
{
    QString request;
    //HTTP GET запрос через браузер
    if(str.startsWith("GET") && str.contains("HTTP"))
    {
        return TcpRequest("HTTP",str.mid(QString("GET").length(),str.indexOf("HTTP")-4).trimmed());
    }

    if(str.startsWith("@API_QT") && str.contains("@END"))
    {
        return TcpRequest("API",str.mid(QString("@API_QT").length(),str.indexOf("@END")-
                                        QString("@API_QT").length()).trimmed());
    }

    return  TcpRequest();
}


