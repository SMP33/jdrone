#ifndef HANDLERCONTROLLER_H
#define HANDLERCONTROLLER_H

#include <QObject>
#include <QTcpSocket>
#include "clienthandlers/abstractclienthandler.h"

class HandlerController : public QObject
{
    Q_OBJECT
public:
    explicit HandlerController(AbstractClientHandler* handler,
                               QTcpSocket* socket,
                               QObject *parent = nullptr);
public slots:
    void readFromSocket();
    void readFromHandler(const QByteArray& bytes);
    void onSocketClosed();
    void closeSocket();

signals:
    void writeToHandler(const QByteArray& bytes);
    void socketClosed();

private:
    AbstractClientHandler* handler;
    QTcpSocket *socket;

};

#endif // HANDLERCONTROLLER_H
