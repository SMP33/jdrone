#include "TcpServer.h"


TcpServer::TcpServer(quint16 port, QObject *parent):
    QTcpServer(parent),
    port(port),
    mutex(new QMutex)
{
    setMaxPendingConnections(10);
}

void TcpServer::start()
{
    assert(this->listen(QHostAddress::Any,port));

    connect(this,&QTcpServer::newConnection,
            this,&TcpServer::newConnection);
}

void TcpServer::newConnection()
{
    QTcpSocket* socket = this->nextPendingConnection();

    connect(socket,&QTcpSocket::readyRead,
            this,&TcpServer::firstReadClient);
}

void TcpServer::firstReadClient()
{
    QTcpSocket* socket = (QTcpSocket*)sender();
    QByteArray buf;

    while (socket->bytesAvailable()>0) {
        buf.append(socket->readAll());
    }

    qDebug()<<"Set handler";

    Request request=extractRequest(buf);

    QStringList l;
    l.append(request.type);
    l.append(request.content);

    if(onNewConnection.contains(l))
    {
        onNewConnection[l](socket,request);
    }
    else
        socket->close();

//    QByteArray response= "HTTP/1.1 200 OK\r\n"\
//                                 "Content-Type: text/html; charset=utf-8\r\n"\
//                                 "\r\n\r\n"\
//                                 "<html>\r\n"\
//                                 "<head>"\
//                                  "<title>Go out of here!</title>"\
//                                 "<style>"\
//                                 ".text {"\
//                                 "text-align:  center;"\
//                                 "}"\
//                                 "</style>"\
//                                 "</head>"\
//                                 "<body>"\
//                                 "<div class=\"text\">"\
//                                 "<h1>404</h1>"\
//                                 "<h3>Watafak are you doing maaaaan?<br>"\
//                                 "What is the</h3><h2>"+request.content.toUtf8()+"?</h2><h3><br>"\
//                                 "Get out, this is not the page you need!<br><br>"
//                                                       "<a href=\"/\">Main page<a></h3>"\
//                                 "</div>"\
//                                 "</body>"\
//                                 "</html>";

//    socket->write(response);
//    socket->flush();
//    socket->close();

    disconnect(socket,&QTcpSocket::readyRead,
               this,&TcpServer::firstReadClient);
}

TcpServer::Request TcpServer::extractRequest(QString str)
{
    QString request;
    //HTTP GET запрос через браузер
    if(str.startsWith("GET") && str.contains("HTTP"))
    {
        return
            Request("HTTP",
                    str.mid(QString("GET").length(),str.indexOf("HTTP")-4).trimmed(),
                    request);
    }
    //API запрос запрос через браузер
    if(str.startsWith("@API_QT") && str.contains("@END"))
    {
        return
            Request("API",
                    str.mid(QString("@API_QT").length(),str.indexOf("@END")-QString("@API_QT").length()).trimmed(),
                    request);
    }

    return  Request();
}

TcpServer::Request::Request(QString type, QString content,QString fullData):
    type(type),
    content(content),
    fullData(fullData)
{

}
