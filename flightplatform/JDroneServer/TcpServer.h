#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QTcpServer>
#include <QTcpSocket>
#include <QStringList>

#include "clienthandlers/abstractclienthandler.h"

class TcpServer : public QTcpServer
{
    Q_OBJECT
public:

    struct Request
    {
        explicit Request(QString type="",QString content="",QString fullData="");

        QString type;
        QString content;
        QString fullData;

    };


    explicit TcpServer(quint16 port,QObject *parent = nullptr);

    template<typename Functor>
    void addConnectionHandler(Request request,Functor functor);

//    template<typename Functor>
//    static std::function<void(bool)> onEntry(Functor functor)
//    {
//        return [functor](bool isEnteringState) {
//            if (isEnteringState)
//                functor();
//        };
//    }

public slots:
    void start();
    void newConnection();
    void firstReadClient();

private:
    quint16 port;
    QMutex* mutex;

    QMap<QStringList,std::function<void(QTcpSocket*,const Request&)>> onNewConnection;

    static Request extractRequest(QString str);
};

template<typename Functor>
void TcpServer::addConnectionHandler(TcpServer::Request request, Functor functor)
{
//    onNewConnection.insert(request,[functor](QTcpSocket* socket,const Request& request) {
//        functor(socket,request);
//    });

QStringList l;
l.append(request.type);
l.append(request.content);

    onNewConnection.insert(l,[functor](QTcpSocket* socket,const TcpServer::Request& request){
    functor(socket,request);
});
}

#endif // TCPSERVER_H
