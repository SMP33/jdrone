#ifndef JDroneServer_H
#define JDroneServer_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QTcpServer>
#include <QTcpSocket>

#include "clienthandlers/BrowserVideoClientHandler.h"

struct TcpRequest
{
    explicit TcpRequest(QString type="",QString content=""):
        type(type),
        content(content)
    {

    }

    QString type;
    QString content;
};

class JDroneServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit JDroneServer(quint16 port,QString main_page=" ",QObject *parent = nullptr);
    int getPort() const;

public slots:
    void start();
    void newConnection();
    void firstReadClient();
    void writeToSocket(const QByteArray &bytes);
    void closeSocket();
    void deleteHandlerFromMap();

signals:
    void frameUpd(const VideoFrame& frame);

private:
    quint16 port;
    QMutex* mutex;
    QString main_page;
    QMap<AbstractClientHandler*,QTcpSocket*> handlers;
    void setClientHandler(TcpRequest request,QTcpSocket* socket);
    static TcpRequest extractRequest(QString str);


};



#endif // JDroneServer_H
