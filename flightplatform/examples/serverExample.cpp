#include <qdebug.h>
#include <QCoreApplication>
#include <QThread>
#include <QFile>
#include <QDateTime>
#include <QLoggingCategory>

#include "../JDroneServer/jdroneserver.h"
#include "../JDroneServer/TcpServer.h"
#include "../ArucoMapDetector/arucomapdetector.h"
#include "../../utils/Utils.h"
#include "../JDroneServer/HandlerController.h"

using namespace std;
QScopedPointer<QFile>   m_logFile;
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

int main(int argc, char** argv)
{
    QCoreApplication app(argc,argv);

    AppProperty prors(argc,argv);

    ArucoMapDetector detector(prors);

    QThread thr2;
    detector.moveToThread(&thr2);

    QObject::connect(&thr2,&QThread::started,
                     &detector,&ArucoMapDetector::start);

    thr2.start();

    int port =8080;

    //DPRINT(port);
    m_logFile.reset(new QFile("logFile.txt"));
    m_logFile.data()->open(QFile::Append | QFile::Text);
    qInstallMessageHandler(messageHandler);

    QLoggingCategory  m_category("Test");
    qInfo(m_category)<<"qInfo Check";
    qDebug()<<"qDebug Check";

    TcpServer server(port,&app);

    TcpServer::Request r("HTTP","/");

    server.addConnectionHandler(r,[](QTcpSocket* socket,const TcpServer::Request& request){
        QByteArray response="HTTP/1.1 200 OK\r\n"
                              "Content-Type: text/html; charset=utf-8\r\n"
                              "\r\n\r\n"
                              "<html>\r\n"
                              "<head>"
                              "<title>JDrone Main Page</title>"
                              "<style>"
                              ".text {"
                              "text-align:  center;"
                              "}"
                              "</style>"
                              "</head>"
                              "<body>"
                              "<div class=\"text\">"
                              "<h1>This is JDroneServer</h1>"
                              "<a href=\"video_stream.mjpeg\">Video stream<a><br>"
                              "<a href=\"telemetry_stream\">Telemetry stream<a><br>"
                              "</div>"
                              "</body>"
                              "</html>";

            socket->write(response);
            socket->flush();
            socket->close();
    });

    server.addConnectionHandler(TcpServer::Request("HTTP","/video_stream.mjpeg"),[&](QTcpSocket* socket,const TcpServer::Request& request){
        BrowserVideoClientHandler* handler =
            new BrowserVideoClientHandler("Video Stream");
        //HandlerController* controller =
            new HandlerController(handler,socket);

            QObject::connect(&detector,&ArucoMapDetector::frameUpdated,
                             handler,&BrowserVideoClientHandler::frameUpd);

            handler->start();

    });

    server.start();

    return app.exec();
}

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // Открываем поток записи в файл
    QTextStream out(m_logFile.data());

    // Записываем дату записи
    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");

    // Записываем в вывод категорию сообщения и само сообщение
    out << context.category << ": "
        << msg << endl;
    out.flush();    // Очищаем буферизированные данные
}
