#include <qdebug.h>
#include <QCoreApplication>
#include <QThread>

#include "../ArucoMapDetector/arucomapdetector.h"
using namespace std;

int main(int argc, char** argv)
{
    QCoreApplication app(argc,argv);
    AppProperty prors(argc,argv);

    ArucoMapDetector detector(prors);

    QThread thr;
    detector.moveToThread(&thr);

    QObject::connect(&thr,&QThread::started,
                     &detector,&ArucoMapDetector::start);

    QObject::connect(&detector,&ArucoMapDetector::positionUpdated,
                     [](const ArucoData& a)
    { qDebug()<<"Pos: [ "<<a.position.x<<" "<<a.position.x<<" "<<a.position.z<<"]"<<" Yaw: "<<a.position.yaw*GM::RAD2DEG<<" FPS: "<<a.fps; }
    );

    thr.start();

    return app.exec();
}
