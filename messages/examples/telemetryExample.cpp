#include <iostream>
#include "../../messages/Messages.h"

#include <QDebug>
#include <QMetaEnum>
#include <QJsonDocument>

using namespace std;




int main(int argc, char** argv)
{
    QJsonObject j;
    j.insert("DJITelemetry",DJITelemetry());

    std::cout<<QJsonDocument(j).toJson().toStdString();

    return 0;
}
