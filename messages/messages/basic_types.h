#ifndef BASIC_TYPES_H_1
#define BASIC_TYPES_H_1

#include <QMetaType>
#include <QDataStream>
#include <QJsonObject>

#define JSON(json,var)\
    json[#var]=var

#define JQ_USER_METATYPE(X) Q_DECLARE_METATYPE(X)\
    static int  X ## _qMetaType_id=qRegisterMetaType<X>(#X);

struct V2Q
{
    V2Q()=default;
    V2Q(qreal x,qreal y):
        x(x),
        y(y)
    {
    }

    qreal x=0;
    qreal y=0;

    friend QDataStream & operator<<(QDataStream & stream, const V2Q & it)
      {
          stream << it.x<<it.y;

          return stream;
      }

    friend QDataStream & operator>>(QDataStream & stream,V2Q & it)
      {
          stream >> it.x>>it.y;

          return stream;
      }

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,x);
        JSON(j,y);
        return j;
    }

};

struct V3Q:V2Q
{
    V3Q()=default;
    V3Q(qreal x,qreal y,qreal z):
        V2Q(x,y),
        z(z)
    {

    }
    qreal z=0;

    friend QDataStream & operator<<(QDataStream & stream, const V3Q & it)
     {
         stream << it.x<<it.y<<it.z;

         return stream;
     }

    friend QDataStream & operator>>(QDataStream & stream,V3Q & it)
     {
         stream >> it.x>>it.y>>it.z;

         return stream;
     }

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,x);
        JSON(j,y);
        JSON(j,z);
        return j;
    }

};



struct AcfPosition:V3Q
{
    AcfPosition()=default;
    AcfPosition(qreal x,qreal y,qreal z,qreal yaw):
        V3Q(x,y,z),
        yaw(yaw)
    {

    }

    qreal yaw=0;

    friend QDataStream & operator<<(QDataStream & stream, const AcfPosition & it)
     {
         stream << it.x<<it.y<<it.z<<it.yaw;

         return stream;
     }

    friend QDataStream & operator>>(QDataStream & stream,AcfPosition & it)
     {
         stream >> it.x>>it.y>>it.z>>it.yaw;

         return stream;
     }

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,x);
        JSON(j,y);
        JSON(j,z);
        JSON(j,yaw);
        return j;
    }

};



struct V2GeoQ
{
    V2GeoQ()=default;
    V2GeoQ(qreal lat, qreal lon):
        lat(lat),
        lon(lon)
    {}
    qreal lat=0;
    qreal lon=0;

    friend QDataStream & operator<<(QDataStream & stream, const V2GeoQ & it)
      {
          stream << it.lat<<it.lon;

          return stream;
      }

    friend QDataStream & operator>>(QDataStream & stream,V2GeoQ & it)
      {
          stream >> it.lat>>it.lon;

          return stream;
      }

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,lat);
        JSON(j,lon);
        return j;
    }

};

struct V3GeoQ:V2GeoQ
{
    V3GeoQ()=default;
    V3GeoQ(qreal lat, qreal lon, qreal alt):
        V2GeoQ(lat,lon),
        alt(alt)
    {}
    qreal alt=0;

    friend QDataStream & operator<<(QDataStream & stream, const V3GeoQ & it)
      {
          stream << it.lat<<it.lon;

          return stream;
      }

    friend QDataStream & operator>>(QDataStream & stream,V3GeoQ & it)
      {
          stream >> it.lat>>it.lon;

          return stream;
      }

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,lat);
        JSON(j,lon);
        JSON(j,alt);
        return j;
    }

};

struct AcfGeoPosition:V3GeoQ
{
    AcfGeoPosition()=default;
    AcfGeoPosition(qreal lat,qreal lon,qreal alt,qreal yaw):
        V3GeoQ(lat,lon,alt),
        yaw(yaw)
    {

    }

    qreal yaw=0;

    friend QDataStream & operator<<(QDataStream & stream, const AcfGeoPosition & it)
     {
         stream << it.lat<<it.lon<<it.alt<<it.yaw;

         return stream;
     }

    friend QDataStream & operator>>(QDataStream & stream,AcfGeoPosition & it)
     {
         stream >> it.lat>>it.lon>>it.alt>>it.yaw;

         return stream;
     }

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,lat);
        JSON(j,lon);
        JSON(j,alt);
        JSON(j,yaw);
        return j;
    }

};

struct QuatQ
{
    QuatQ()=default;
    QuatQ(qreal x,qreal y,qreal z,qreal w):
        x(x),
        y(y),
        z(z),
        w(w)
    {

    }
    qreal x=0;
    qreal y=0;
    qreal z=0;
    qreal w=0;

    friend QDataStream & operator<<(QDataStream & stream, const QuatQ & it)
     {
         stream << it.x<<it.y<<it.z<<it.w;

         return stream;
     }

    friend QDataStream & operator>>(QDataStream & stream,QuatQ & it)
     {
         stream >> it.x>>it.y>>it.z>>it.w;

         return stream;
     }

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,x);
        JSON(j,y);
        JSON(j,z);
        JSON(j,w);
        return j;
    }

};

JQ_USER_METATYPE(V2Q)
JQ_USER_METATYPE(V3Q)
JQ_USER_METATYPE(AcfPosition)
JQ_USER_METATYPE(V2GeoQ)
JQ_USER_METATYPE(V3GeoQ)
JQ_USER_METATYPE(AcfGeoPosition)
JQ_USER_METATYPE(QuatQ)


#endif // BASIC_TYPES_H
