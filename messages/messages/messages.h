#ifndef MESSAGES_H
#define MESSAGES_H

#include "basic_types.h"

struct ArucoData
{
    ArucoData()=default;
    explicit ArucoData(AcfPosition pos,int fps=0,int markersCount=0):
        position(pos),
        fps(fps),
        markersCount(markersCount)
    {

    }
    AcfPosition position;
    int fps=0;
    int markersCount=0;

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,position);
        JSON(j,fps);
        JSON(j,markersCount);
        return j;
    }

};

struct JDroneMsg
{
    JDroneMsg()=default;
    JDroneMsg(QString type,QByteArray content):
        type(type),
        content(content)
    {
    }

    QString type;
    QByteArray content;
};

struct VideoFrame
{
    VideoFrame()=default;
    VideoFrame(QString type,QByteArray content):
        type(type),
        content(content)
    {
    }

    QString type;
    QByteArray content;
};

struct ModeHandlerMsg
{
    ModeHandlerMsg() {}
    QString mode="";
    QByteArray content;
};

JQ_USER_METATYPE(ArucoData)
JQ_USER_METATYPE(JDroneMsg)
JQ_USER_METATYPE(VideoFrame)

#endif // MESSAGES_H
