#ifndef DJI_TELEMETRY_TYPES_H
#define DJI_TELEMETRY_TYPES_H

#include <QObject>
#include "basic_types.h"
#include <QDataStream>
#include <QMetaEnum>

class DJIEnums{
public:
    enum class FlightState: qint16
    {
            STOPED =0,
            ON_GROUND =1,
            IN_AIR=2
    };
    Q_ENUM(FlightState)

    enum class LandingGearState: qint16
    {
        UNDIFINED = 0,
        DOWN = 1,
        UP = 2,
        IN_MOVE = 3
    };
    Q_ENUM(LandingGearState)

    enum class HorizontalCoordinateSystem: qint16
    {
        FROM_GROUND=0x00,
        FROM_BODY=0x02
    };
    Q_ENUM(HorizontalCoordinateSystem)

    enum class AngularMotion: qint16
    {
        SET_YAW_ANGLE=0x00,
        SET_YAW_RATE=0x08
    };
    Q_ENUM(AngularMotion)

    enum class LinearHorizontalMotion: qint16
    {
        SET_PITCH_ROLL_ANGLE = 0x00,
        SET_X_Y_VELOCITY = 0x40,
        SET_X_Y_OFFSET = 0x80,
        SET_PITCH_ROLL_ANGULAR_RATE = 0xC0
    };
    Q_ENUM(LinearHorizontalMotion)

    enum class LinearVerticalMotion: qint16
    {
        SET_Z_VELOCITY=0x00,
        SET_Z_POSITION=0x10,
        SET_MOTOR_THRUST=0x20
    };
    Q_ENUM(LinearVerticalMotion)

    Q_GADGET
    DJIEnums() = delete;

};

struct DJITelemetry
{
    DJITelemetry()=default;

    V3GeoQ gpsPosition={0,0,0};
    QuatQ quatertion={0,0,0,0};
    V3Q velocity={0,0,0};
    qint16 gpsHealth = 0;
    qint16 gpsSattelitesCount=0;
    qint16 voltage=0;
    bool controlledBySDK=false;
    DJIEnums::FlightState flightState=DJIEnums::FlightState::STOPED;
    DJIEnums::LandingGearState landingGearState=DJIEnums::LandingGearState::UNDIFINED;
    qint64 timestamp = 0;

    friend QDataStream & operator<<(QDataStream & stream, const DJITelemetry & t)
     {
         stream << t.gpsPosition<<t.quatertion<<t.velocity
                <<t.gpsHealth<<t.gpsSattelitesCount<<t.voltage
               <<t.controlledBySDK<<static_cast<qint16>(t.flightState)<<static_cast<qint16>(t.landingGearState)
              <<t.timestamp;

         return stream;
     }

    friend QDataStream & operator>>(QDataStream & stream,DJITelemetry & t)
     {
        quint16 flightState;
        quint16 landingGearState;
        stream >> t.gpsPosition>>t.quatertion>>t.velocity
               >>t.gpsHealth>>t.gpsSattelitesCount>>t.voltage
              >>t.controlledBySDK>>flightState>>landingGearState
             >>t.timestamp;

        t.flightState=static_cast<DJIEnums::FlightState>(flightState);
        t.landingGearState=static_cast<DJIEnums::LandingGearState>(landingGearState);

         return stream;
     }

    operator QJsonValue()
    {
        QJsonObject j;
        JSON(j,gpsPosition);
        JSON(j,quatertion);
        JSON(j,velocity);
        JSON(j,gpsHealth);
        JSON(j,gpsSattelitesCount);
        JSON(j,voltage);
        JSON(j,controlledBySDK);
        j.insert("flightState",static_cast<qint16>(flightState));
        j.insert("landingGearState",static_cast<qint16>(landingGearState));

        j.insert("flightStateStr",
                 QString(QMetaEnum::fromType<DJIEnums::FlightState>().
                         valueToKey(static_cast<quint16>(flightState))));
        j.insert("landingGearStateStr",
                 QString(QMetaEnum::fromType<DJIEnums::LandingGearState>().
                         valueToKey(static_cast<quint16>(landingGearState))));

        return j;
    }

};

JQ_USER_METATYPE(DJITelemetry)

#endif // DJI_TELEMETRY_TYPES_H
