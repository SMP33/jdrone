#include "appproperty.h"

#include <fstream>
#include <iostream>

#include <qdebug.h>
#include <qfileinfo.h>
#include <qfile.h>
#include <qregularexpression.h>



AppProperty::AppProperty(int argc,char** argv,std::string configPath):
        argc(argc),
        argv(argv)
    {
        QFileInfo info(QString::fromStdString(configPath));
        QFile f(info.absoluteFilePath());

        props["EXECUTABLE_PATH"]=info.absolutePath();

        if(!f.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            std::cout<<"File cannot be opened!!!"<<std::endl;
            return;
        }

        QString text;

        QString lastKey;

        while (!f.atEnd()) {
            QString line = f.readLine();
            QString tmp=line.trimmed();

            if(!tmp.startsWith("@PROPERTY"))
            {
                if(tmp.startsWith("#"))
                    continue;
                else
                {
                    line=line.replace("\\#","#");
                    line=line.replace("\\@PROPERTY","@PROPERTY");
                    if(lastKey.length()!=0)
                        props[lastKey]+=line;
                }
            }
            else
            {
                tmp=tmp.replace("@PROPERTY","");


                QString key;

                key=tmp.split("=")[0];
                key=key.trimmed();
                props[key]=tmp.split("=")[1];
                lastKey=key;

            }

        }

    }
