#ifndef APPPROPERTY_H
#define APPPROPERTY_H

#include <QMap>

struct AppProperty
{
    explicit AppProperty(int argc,char** argv,std::string configPath="config.txt");
    int argc;
    char** argv;
    QMap<QString,QString> props;
};

#endif // APPPROPERTY_H
