#include <iostream>
#include "../../utils/Utils.h"

#include <qdebug.h>

using namespace std;

int main(int argc, char** argv)
{
    JDProtocolEncoder encoder;
    JDProtocolDecoder decoder;

    QByteArray data="asdasdasdasd@!B@B21,456123,<transported_data>@!B456123CS@E";
    encoder.encode({data.begin(),data.end()});
    auto src_msg=encoder.getCurrentMsg();

    DPRINT(QByteArray(reinterpret_cast<const char*>(src_msg.data()), src_msg.size()));
    data="asdasdasdasd@!B@B21,456123,<data>@!B456123CS@E_";
    encoder.encode({data.begin(),data.end()});
    src_msg=encoder.getCurrentMsg();

    DPRINT(QByteArray(reinterpret_cast<const char*>(src_msg.data()), src_msg.size()));

    for(const auto& b:src_msg)
    {
        decoder.readByte(b);
    }

    auto buf=decoder.getLastMsg();
    QByteArray msg = QByteArray(reinterpret_cast<const char*>(buf.data()), buf.size());
    DPRINT(msg);
    DPRINT(decoder.getLastMsgId());

    return 0;
}
