#ifndef JDRONE_MACRO_H
#define JDRONE_MACRO_H

#include <QDebug>
#include <QString>

#define DPRINT(var) qDebug().noquote().nospace()<< (QString(#var )+":").toUtf8()<<"\""<<var<<"\"";

#endif // JDRONE_MACRO_H
