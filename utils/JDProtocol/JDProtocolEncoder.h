#ifndef JDPROTOCOLENCODER_H
#define JDPROTOCOLENCODER_H

#include <stdio.h>
#include <vector>
#include "JDProtocolChecksum.h"

class JDProtocolEncoder
{
public:
    JDProtocolEncoder();
    ~JDProtocolEncoder();
    void encode(const std::vector<uint8_t>& data);
    unsigned int getCurrentMsgId() const;
    std::vector<uint8_t> getCurrentMsg() const;

private:
    unsigned int currentMsgId;
    std::vector<uint8_t> currentMsg;
    std::vector<uint8_t> msgBuffer;

    char* header;
    char* footer;
};

#endif // JDPROTOCOLENCODER_H
