#ifndef JDPROTOCOLCHECKSUM_H
#define JDPROTOCOLCHECKSUM_H

#include <cstdint>

class JDProtocolChecksum
{
public:
    JDProtocolChecksum();
    void addByte(char b);
    void reset();
    uint16_t getValue() const;

    const static uint8_t magifCoef=5;
private:
    uint16_t value;
    uint8_t i;

};

#endif // JDPROTOCOLCHECKSUM_H
