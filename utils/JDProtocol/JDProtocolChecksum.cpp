#include "JDProtocolChecksum.h"

JDProtocolChecksum::JDProtocolChecksum():
    value(0),
    i(0)
{

}

void JDProtocolChecksum::addByte(char b)
{
    value+=(b+i)*(magifCoef+1);
    i++;
}

void JDProtocolChecksum::reset()
{
    i=0;
    value=0;
}

uint16_t JDProtocolChecksum::getValue() const
{
    return value;
}
