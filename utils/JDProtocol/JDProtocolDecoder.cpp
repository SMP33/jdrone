#include "JDProtocolDecoder.h"
#include "macro.h"

JDProtocolDecoder::JDProtocolDecoder():
    buf(0),
    b_last(' '),
    lastMsg(0),
    lastMsgId(0)
{

}

bool JDProtocolDecoder::readByte(uint8_t b)
{
    if(b_last=='@' && b=='B')
    {
        startMessage();
        b_last=b;
        return false;
    }
    else if (b_last=='@' && b=='E')
    {
        b_last=b;
        return endMessage();
    }

    buf.push_back(b);

    b_last=b;

    return false;
}

std::vector<uint8_t> JDProtocolDecoder::getLastMsg() const
{
    return lastMsg;
}

unsigned int JDProtocolDecoder::getLastMsgId() const
{
    return lastMsgId;
}

void JDProtocolDecoder::startMessage()
{
    buf.clear();
}

bool JDProtocolDecoder::endMessage()
{
    int asd=0;

    DPRINT(QByteArray(reinterpret_cast<const char*>(buf.data()), buf.size()));

qDebug()<<asd++;

    long bs=buf.size();

    //@B<длина сообщения>,<id>,<передаваемые данные><id><чексумма>@E
    //Находим индексы двух запятых

    long d1=-1;
    long d2=-1;

    for (long i=0;i<bs;i++) {
        if(buf[i]==','){
            d1=i;
            break;
        }
    }
    if(d1<1){buf.clear(); return false;}//минимальный размер сообщения - 1 байт
qDebug()<<asd++;
    for (long i=d1+1;i<bs;i++) {
        if(buf[i]==','){
            d2=i;
            break;
        }
    }
    if(d2<d1+2){buf.clear(); return false;}
qDebug()<<asd++;
    //Находим строки с длиной и id

    long len_str_len=d1;
    long id_str_len=d2-d1-1;

    char* len_str= new char[len_str_len+1];
    char* id_str= new char[id_str_len+1];

    for(long i=0; i<len_str_len;i++)
    {
        len_str[i]=buf[i];
    }
    len_str[len_str_len]='\0';
qDebug()<<asd++;
    for(long i=len_str_len+1; i<d2;i++)
    {
        id_str[i-(len_str_len+1)]=buf[i];
    }
    id_str[id_str_len]='\0';

    //Вычисляем их значения
    long len=atol(len_str);
    long id=atol(id_str);

    //                Длина_сообщения Зпт   ID       Зпт Контент    ID       ЧС  @
    long full_msg_len=  len_str_len   +1 +id_str_len +1   +len   +id_str_len +2 +1;
qDebug()<<asd++;

DPRINT(buf.size());
DPRINT(full_msg_len);
    //Сравнение предполагаемой и реальной длины сообщения
    if(full_msg_len!=buf.size()){buf.clear(); return false;}
qDebug()<<asd++;
    //ID в конце и в начале сообщения должны совпадать
    for(long i=bs-3-id_str_len;i<bs-3;i++)
    {
        if(id_str[i-(bs-3)+id_str_len]!=buf[i])
        {
            buf.clear(); return false;
        }
    }
qDebug()<<asd++;
    //Считаем чексумму и сравниваем ее с полученной
    long contentStart=d2+1;
    JDProtocolChecksum cs;
    for(long i=contentStart;i<contentStart+len;i++)
    {
        cs.addByte(buf[i]);
    }

    uint16_t cheksumNeed=cs.getValue();
    uint16_t cheksumReal=(buf[bs-3] << 8) | buf[bs-2];

    DPRINT(cheksumNeed);
    DPRINT(cheksumReal);

    //if(cheksumNeed!=cheksumReal){buf.clear(); return false;}

    //Если все проверки пройдены, обновляем сообщение
    lastMsgId=id;

    lastMsg.clear();
    for(long i=contentStart;i<contentStart+len;i++)
    {
        uint8_t& b=buf[i];

        if(b!='@') {lastMsg.push_back(b);}
        else if (buf[i+1]=='!') {
            lastMsg.push_back(b);
            i++;
        }


    }


    buf.clear();
    return false;
}






































