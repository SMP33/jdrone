#include "JDProtocolEncoder.h"
#include <macro.h>

JDProtocolEncoder::JDProtocolEncoder():
    currentMsgId(0),
    currentMsg(0),
    header(new char[50]),
    footer(new char[30])
{

}

JDProtocolEncoder::~JDProtocolEncoder()
{
    delete[] header;
    delete[] footer;
}

void JDProtocolEncoder::encode(const std::vector<uint8_t>& data)
{
    currentMsgId++;

    currentMsg.clear();
    msgBuffer.clear();

    for(const auto& b:data)
    {
        if(b!='@')
            msgBuffer.push_back(b);
        else
        {
            msgBuffer.push_back('@');
            msgBuffer.push_back('!');
        }
    }

    JDProtocolChecksum cs;

    for(const auto& b:msgBuffer)
    {
        cs.addByte(b);
    }

    sprintf(header,"@B%ld,%u,",msgBuffer.size(),currentMsgId);
    sprintf(footer,"%u%c%c@E",currentMsgId,cs.getValue()>>8,cs.getValue());

    long i=0;
    while(header[i])
    {
        currentMsg.push_back(header[i]);
        i++;
    }

    for (const auto& b : msgBuffer) {
        currentMsg.push_back(b);
    }

    i=0;
    while(footer[i])
    {
        currentMsg.push_back(footer[i]);
        i++;
    }

    DPRINT(header);
    DPRINT(footer);

}

unsigned int JDProtocolEncoder::getCurrentMsgId() const
{
    return currentMsgId;
}

std::vector<uint8_t> JDProtocolEncoder::getCurrentMsg() const
{
    return currentMsg;
}
