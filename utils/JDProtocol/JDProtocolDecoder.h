#ifndef JDPROTOCOL_DECODER_H
#define JDPROTOCOL_DECODER_H

#include <stdio.h>
#include <vector>
#include "JDProtocolChecksum.h"

class JDProtocolDecoder
{
public:
    JDProtocolDecoder();
    bool readByte(uint8_t b);
    std::vector<uint8_t> getLastMsg() const;
    unsigned int getLastMsgId() const;

private:
    std::vector<uint8_t> buf;
    char b_last;

    std::vector<uint8_t> lastMsg;
    unsigned int lastMsgId;

    void startMessage();
    bool endMessage();
};

#endif // JDPROTOCOL_DECODER_H

//@B<длина сообщения>,<id>,<передаваемые данные><id><чексумма>@E
